
const mongoose = require("mongoose");

//create a schema for blog
const blogSchema = new mongoose.Schema({
	sTitle: {
		type: String,
		required: [true, "Title is required"],
		minlength:  [1, "Description must be at least 1 characters long"],
		trim:true,
		unique:true
	},
	sDescription: {
		type: String,
		required: [true, "Descreption is required"],
		minlength: [10, "Description must be at least 10 characters long"],
		trim:true
	},
	sPhoto:{
		type: String,
	},
	sPostedBy: {
		type: String
 
	},
	dPublishedDate: {
		type: String,
		default: `${new Date().getMonth()+1}/${new Date().getDate()}/${new Date().getFullYear()}`,
		validate: {
			validator: function (v) {
                 
                
				let publishedDate = new Date(v);
				if(publishedDate.getDate() >= new Date().getDate() && publishedDate.getMonth() >= new Date().getMonth() && publishedDate.getFullYear() >= new Date().getFullYear()){
					return true;
				}
				return false;
			},
			message: " Published Date must be greater than or equals to today's date"
		},
       
       
	},
	bPublished:
    {
    	type: Boolean,
    	default: false
    },
	aLikes: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	}],
   dCreatedAt: {
		type: String,
		default:dateFormater()
	},
});

function dateFormater(){
	 let date = new Date();
	 let resdate;
	 date.getMonth().toString().length ==1 ? resdate = "0"+parseInt(date.getMonth()+1) : resdate =date.getMonth().toString();
	 date.getDate().toString().length ==1 ? resdate += "/0"+date.getDate().toString() : resdate += "/"+ date.getDate().toString();

	 return resdate += "/"+date.getFullYear().toString();
}

blogSchema.index({sTitle: "text", sDescription: "text",sPostedBy: "text"});
const Blog = mongoose.model("Blog", blogSchema);

module.exports = Blog;