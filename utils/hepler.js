//require bcrypt
const bcrypt = require("bcryptjs");  //require bcrypt
// const sgMail = require("@sendgrid/mail");
require('dotenv').config();
// console.log(process.env.sendGrid_Api_key);
// sgMail.setApiKey(process.env.sendGrid_Api_key);
let helper = {};

helper.genrateHash=(plainPassword)=>{
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(plainPassword, salt);
    return hash;
};

helper.comparePassword=(plainPassword, hashPassword)=>{
    return bcrypt.compareSync(plainPassword, hashPassword);
};



// helper.sendEmail = (receiver, source, subject, content) => {
//     try {
//         const data = {
//             to: receiver,
//             from: source,
//             subject,
//             html: content,
//         };
//         return sgMail.send(data);
//     } catch (e) {
//         return new Error(e.message);
//     }
// };
 

 


module.exports = helper;

