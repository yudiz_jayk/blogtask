let userController = {};
const _ = require("lodash");
const formidable = require("formidable");
const User = require("../models/user");
let errorHandler = require("../utils/errorHandler");

var crypto = require("crypto");
var nodemailer = require("nodemailer");
var frontEndUrl = "http://localhost:3000/forgot-password";
let cloudinary = require("cloudinary");
let helper = require("../utils/hepler");
cloudinary.config({
	cloud_name: "dc1ck5lh8",
	api_key: "975538678341646",
	api_secret: "-EZkzbOSM_53CT_-RX8PYnaspqc"
});


let transporter = nodemailer.createTransport({
	host: "smtp.gmail.com",
	port: 587,
	secure: false, // true for 465
	auth: {
		user: "jaykaravadra77@gmail.com", // your gmail email adress
		pass: "jay@3454"// your gmail password
	}
});

function verifySmtp() {
	// verify connection configuration
	transporter.verify(function (error) {
		if (error) {
			//dis
			console.log(error);
		} else {
			console.log("Server is ready for emails");
		}
		return true;
	});
}
verifySmtp();
userController.changeprofile = (req, res) => {
    
	let user = req.user;
	try {
		let form = new formidable.IncomingForm();
		form.parse(req, async (err, fields, files) => {

			try {
				_.extend(user, fields);
				let nuser= await User.findByIdAndUpdate(user._id,  user,{runValidators:true});
            

				/* eslint-disable */
				async function deleteFile() {
					let split = user.sPhoto.split("/");
					let folder = split[split.length - 2];
					let file = split[split.length - 1].split(".")[0];
					let file_path = folder + "/" + file;
					const result = await cloudinary.uploader.destroy(file_path);
					nuser.sPhoto = "";
               /* eslint-enable */
                 
				}

				if (err) return res.status(500).send({ message: "err in form parsing" });
				const isFileValid = (file) => {
					const type = file.mimetype.split("/").pop();
					const validTypes = ["jpg", "jpeg", "png"];
					if (validTypes.indexOf(type) === -1) {
						return false;
					}
					return true;
				};

				if (!files.sPhoto) {
					if(user.sPhoto){
						deleteFile();
                
					}
                
				}

				if (files.sPhoto && !files.sPhoto.length) {
					const file = files.sPhoto;
					if (isFileValid(file)) {
						if (file.size > 2000000) throw new Error("File size is too big (max 2MB)");
						if (user.sPhoto)deleteFile();
                        
						const result = await cloudinary.v2.uploader.upload(file.filepath, {
							folder: "profilePics",
						});
                    
						nuser.sPhoto = result.url;
                        
					} else {
						throw new Error("Invalid file type");
					}
				} 
				await nuser.save();
				return res.status(200).send({ message: "Profile Updated Successfully" });

			} catch (error) { 
				errorHandler(req, res, error);
			}

		});

	} catch (error) {
		errorHandler(req, res, error);
	}
};

userController.linkforChanePassword = async (req, res) => {
	try {

		crypto.randomBytes(32, async (err, buffer) => {
			if (err) {
				return res.status(500).send({ message: "Error in generating token" });
			}
			const token = buffer.toString("hex");
			let user = await User.findOne({ sEmail: req.body.sEmail });
			if (!user) return res.status(400).send({message:"No user with this email"});

			user.reqPasswordchangeToken = token;
			user.expireToken = Date.now() + 3600000;

			await user.save();

			let html = `
            <p>no reply</p>
            <a href="${frontEndUrl}/${token}">Reset Password</a>
            <p>This link will expire in 1 hour</p>
            `;
			let info = await transporter.sendMail({
				from: "jay", 
				to: req.body.sEmail, 
				subject: "for reset Password ✔",  
				text: "Link expiredd in 1 hour",  
				html: html, 
			});
			if(!info) return res.status(500).send({message:"Error in sending mail"});
			console.log("password reset link sent to your email");
			return res.status(200).send({ message: "Email sent Please check your mail" });
		});
	} catch (error) {
		errorHandler(req,res,error);
	}
};

userController.newPassword = async (req, res) => {

	let { sPassword, token } = req.body;
	try {
		let user = await User.findOne({ reqPasswordchangeToken: token, expireToken: { $gt: Date.now() } });
		if (!user) return res.status(400).send("Invalid Token");
         

		user.reqPasswordchangeToken = undefined;
		user.expireToken = undefined;

		await User.findByIdAndUpdate(user._id, { $unset: { reqPasswordchangeToken: 1, expireToken: 1 }, $set: { sHash: helper.genrateHash(sPassword) } });
		res.status(200).send({message:"Password changed successfully"});
	} catch (error) {
		errorHandler(req,res,error);
	}
    
};

userController.changePassword = async (req, res) => {
	try {
		let {sPassword} = req.body;
		await User.findByIdAndUpdate(req.user._id, { $set: { sHash: helper.genrateHash(sPassword) } }); 
		return res.status(200).send({message:"Password changed successfully"});
	} catch (error) {
		errorHandler(req,res,error); 
	}
};         
        
 




module.exports = userController;