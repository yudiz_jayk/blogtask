let adminController = {};
let User = require("../models/user");
const errorHandler = require("../utils/errorHandler");
let formidable  = require("formidable");
let Blog = require("../models/blog");
let helper = require("../utils/hepler");
let cloudinary = require("cloudinary");
 
cloudinary.config({
	cloud_name: "dc1ck5lh8",
	api_key: "975538678341646",
	api_secret: "-EZkzbOSM_53CT_-RX8PYnaspqc"
});
adminController.getUsers = async(req, res) => {
	try {
    
		req.query.sortBy = req.query.sortBy || "createdAt";
		let order = req.query.order || "asc";
		let query =req.query.sortBy;

		let users = await User.find({role:0}).sort({[query]:parseInt(order)});
		return res.status(200).send({message:"Users fetched successfully",users});
	} catch (error) {
		errorHandler(req,res,error);
	}

};

adminController.deleteUser = async(req, res) => {
	try {
		let user= await User.findByIdAndDelete(req.params.id);
		if(!user)return res.status(400).send({message:"User not found"});
		return res.status(200).send({message:"User deleted successfully"});
	} catch (error) {
		errorHandler(req,res,error);
	}
};


adminController.blogposts = async(req, res) => {
	try {
    
		let bPublised = req.query.bPublished || true;
		let blogsByuser= req.query.blogsByuser;
		let skip = req.query.skip || 0;
		let limit = req.query.limit || 2;
		if(blogsByuser){
			let blogs = await Blog.find({oPostedBy:blogsByuser,bPublished:bPublised}).sort({dPublishedDate:-1}).skip(skip).limit(limit);
			if(blogs.length == 0)return res.status(400).send({message:"Blog not found"});
			return res.status(200).send({message:"Posts fetched successfully",blogs});
		}else{
			let blogs = await Blog.find({bPublished:bPublised}).skip(skip).limit(limit);
			return res.status(200).send({message:"Posts fetched successfully",blogs});
		}
   
        
        
	} catch (error) {
		errorHandler(req,res,error);
	}
};

adminController.searchUser = async(req, res) => {
	try {
		let searchKeys = {sUsername:"sUsername",sEmail:"sEmail",_id:"_id"};
		let key =Object.keys(req.body)[0];
		if(searchKeys[key]){
			let users = await User.find({[key]:req.body[key]});
			if(users.length==0)return res.status(400).send({message:"User not found"});
			return res.status(200).send({message:"Users fetched successfully",users});
		}else{
			return res.status(400).send({message:"Invalid search key"});
		}
	} catch (error) {
		errorHandler(req,res,error);
	}
};

adminController.dashboard = async(req, res) => {
	let limit =parseInt( req.query.limit || 2);
	let skip =parseInt( req.query.skip || 0);
    let searchValue = req.query.searchValue || "";
 
	let published = JSON.parse(req.query.published || true);
	if(searchValue)var searchVal = {$text:{$search:searchValue}};
	else var searchVal = {null:null};

	let rangeFilter = {null:null} ;
	let createAtfilter = {null:null} ;

	if(req.query.rangepub){
		 let rangepub = req.query.rangepub.split("-");
		 let lowRange = rangepub[0];
		 let highRange = rangepub[1];
		 if(!lowRange || !highRange)return res.status(400).send({message:"Please provide date range"});
		 if(parseInt(lowRange) >parseInt(highRange))return res.status(400).send({message:"Low range should be less than high range"});
		 rangeFilter = {dPublishedDate:{$gte:lowRange,$lte:highRange}};
	}
	 if(req.query.createrange){
		 let createrange = req.query.createrange.split("-");
		 let lowRange = createrange[0];
		 let highRange = createrange[1];
		 if(!lowRange || !highRange)return res.status(400).send({message:"Please provide date range"});
		 if(parseInt(lowRange) >parseInt(highRange))return res.status(400).send({message:"Low range should be less than high range"});
		 createAtfilter = {dCreatedAt:{$gte:lowRange,$lte:highRange}};
	}
    
    let sortBy = req.query.sortBy || "aLikes";
	let order = req.query.order || 1;
 
	try {
	     let blogs = await Blog.aggregate([
				{$match : {$and:[{bPublished:published},searchVal]}},
				 {$match:rangeFilter},
				 {$match:createAtfilter},
				 {$project:{_id:1,sTitle:1,sDescription:1,dCreatedAt:1,dPublishedDate:1,oPostedBy:1,bPublished:1,likes:{$size:"$aLikes"}}},
				 {$sort:{[sortBy]:parseInt(order)}},
				 {$skip:skip},
				 {$limit:limit},
			 ])
			 let totalBlogs = await Blog.aggregate([
				{$match : {$and:[{bPublished:published},searchVal]}},
				 {$match:rangeFilter},
				 {$match:createAtfilter},
				 {$project:{_id:1,sTitle:1,sDescription:1,dCreatedAt:1,dPublishedDate:1,oPostedBy:1,bPublished:1,likes:{$size:"$aLikes"}}},
				 {$sort:{[sortBy]:parseInt(order)}},
			 ])
			 
		 
       return res.status(200).send({message:"Posts fetched successfully",totalNumberofblogs:totalBlogs.length,blogs});
	} catch (error) {
		errorHandler(req,res,error);
	}
};
 
adminController.filterbydate = async(req, res) => {
	try {
		let lowRange = req.query.datefrom;
		let highRange = req.query.dateto;
		console.log(lowRange,highRange);
		if(!lowRange || !highRange)return res.status(400).send({message:"Please provide date range"});
      
		if(parseInt(lowRange) >parseInt(highRange))return res.status(400).send({message:"Low range should be less than high range"});

		let datelowRange = new Date();
		datelowRange.setDate(lowRange);
		let datehighRange = new Date();
		datehighRange.setDate(highRange);

     
		let blogs = await Blog.find({dPublishedDate:{$gte:datelowRange,$lte:datehighRange}});
		if(blogs.length == 0)return res.status(400).send({message:"Blog not found within specified date range"});
		return res.status(200).send({message:"Posts fetched successfully",blogs});
	}
	catch (error) {
		errorHandler(req,res,error);
	}
};

adminController.createuser = async (req, res) => {
	try {

      
		let form = new formidable.IncomingForm();
		form.parse(req, async (err, fields, files) => {
			try {
				if (err) return res.status(500).send({ message: "err in form parsing" });
				let { sPassword } = fields;
				if (typeof sPassword != "string") throw new Error("Password is must be string");
				if (!(sPassword && sPassword.trim().length >= 1)) throw new Error("minimum length of password is 1");


				fields.sHash = helper.genrateHash(sPassword);
				delete fields.sPassword;


                
				const isFileValid = (file) => {
					const type = file.mimetype.split("/").pop();
					const validTypes = ["jpg", "jpeg", "png"];
					if (validTypes.indexOf(type) === -1) {
						return false;
					}
					return true;
				};

             
				let user = await User.create(fields);
        
             
				if (files.sPhoto && !files.sPhoto.length) {
                    
					const file = files.sPhoto;
					if (isFileValid(file)) {
						if (file.size > 2000000) throw new Error("File size is too big (max 2MB)");
						const result = await cloudinary.v2.uploader.upload(file.filepath, {
							folder: "profilePics",
						});
						user.sPhoto = result.url;
                       
        
					} else {
						throw new Error("Invalid file type");
					}
				}
				await user.save();
				return res.status(200).send({ message: "Registerd successfully" });

			} catch (error) {
                
				errorHandler(req, res, error);
			}

		});

	} catch (error) {
		errorHandler(req, res, error);
	}
};
module.exports = adminController;